﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController: ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context){
            _context = context;
        }

        [HttpPost("CriarVenda")]
        public IActionResult Criar(Venda venda)
        {

            if(ModelState.IsValid){
                venda.Data = DateTime.Now;
                _context.Vendas.Add(venda);
                _context.SaveChanges();
                return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
            }

            return BadRequest(new { Erro = "A Venda não é válida!" });
        }

        [HttpGet("ObterPorID")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null) {
                return NotFound();
            }

            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            if (vendedor == null) {
                return NotFound();
            }

            var itens = _context.Itens.Where(x => x.Id == id).ToList();

            var v = new Venda()
            {
                Data = venda.Data,
                Status = venda.Status,
                Vendedor = new Vendedor()
                {
                    Nome = vendedor.Nome,
                    Cpf = vendedor.Cpf,
                    Email = vendedor.Email,
                    Telefone = vendedor.Telefone,
                }
            };

            foreach (var item in itens)
            {
                v.ItensVendidos.Add(new Item
                {
                    Id = item.Id,
                    Quantidade = item.Quantidade,
                    Valor = item.Valor,
                    Nome = item.Nome,
                    Descricao = item.Descricao
                });
            }

            return Ok(v);
        }

        [HttpPut("AtualizaStatusVenda")]
        public IActionResult AtualizaStatusVenda(int id, EnumStatusPagamento status)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null){
                return NotFound();
            }

            if (vendaBanco.Status == EnumStatusPagamento.AguardandoPagamento && status == EnumStatusPagamento.PagamentoAprovado
                || vendaBanco.Status == EnumStatusPagamento.AguardandoPagamento && status == EnumStatusPagamento.Cancelada)
            {
                vendaBanco.Status = status;
            }
            else if (vendaBanco.Status == EnumStatusPagamento.PagamentoAprovado && status == EnumStatusPagamento.EnviadoParaTransportadora
                || vendaBanco.Status == EnumStatusPagamento.PagamentoAprovado && status == EnumStatusPagamento.Cancelada)
            {
                vendaBanco.Status = status;
            }
            else if (vendaBanco.Status == EnumStatusPagamento.EnviadoParaTransportadora && status == EnumStatusPagamento.Entregue)
            {
                vendaBanco.Status = status;
            }
            else
            {
                return BadRequest();
            }
           
            _context.Vendas.Update(vendaBanco); 
            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodasVendas()
        {
            var vendas = _context.Vendas.ToList();

            if (vendas == null) {
                return NotFound();
            }

            foreach (var venda in vendas){
                var vendedor = _context.Vendedores.Find(venda.VendedorId);
                
                if (vendedor == null) {
                    return NotFound();
                }

                var itens = _context.Itens.Where(x => x.Id == venda.Id).ToList();

                var v = new Venda()
                {
                    Data = venda.Data,
                    Status = venda.Status,
                    Vendedor = new Vendedor()
                    {
                        Nome = vendedor.Nome,
                        Cpf = vendedor.Cpf,
                        Email = vendedor.Email,
                        Telefone = vendedor.Telefone,
                    }
                };

                foreach (var item in itens)
                {
                    v.ItensVendidos.Add(new Item
                    {
                        Id = item.Id,
                        Quantidade = item.Quantidade,
                        Valor = item.Valor,
                        Nome = item.Nome,
                        Descricao = item.Descricao
                    });
                }
            };

            return Ok(vendas);
        }
    
        [HttpGet("ObterVendaPorVendedor")]
        public IActionResult ObterVendaPorVendedor(int idVendedor){

            var vendas = _context.Vendas.Where(x => x.VendedorId == idVendedor).ToList();
           
            if (vendas == null) {
                return NotFound();
            }

            foreach (var venda in vendas){
                var vendedor = _context.Vendedores.Find(venda.VendedorId);
                
                if (vendedor == null) {
                    return NotFound();
                }

                var itens = _context.Itens.Where(x => x.Id == venda.Id).ToList();

                var v = new Venda()
                {
                    Data = venda.Data,
                    Status = venda.Status,
                    Vendedor = new Vendedor(){
                        Nome = vendedor.Nome,
                        Cpf = vendedor.Cpf,
                        Email = vendedor.Email,
                        Telefone = vendedor.Telefone,
                    }
                };

                foreach (var item in itens)
                {
                    v.ItensVendidos.Add(new Item
                    {
                        Id = item.Id,
                        Quantidade = item.Quantidade,
                        Valor = item.Valor,
                        Nome = item.Nome,
                        Descricao = item.Descricao
                    });
                }
            };

            return Ok(vendas);
        }
    
    }
}