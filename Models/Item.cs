using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Item
    {        
        [Key]
        public int Id { get; set; }
        
        public int VendaId { get; set; }

        [Required]
        public string Nome { get; set; } = string.Empty;

        [Required]
        public string Descricao { get; set; } = string.Empty;

        [Required]
        public double Valor { get; set; }

        [Required]
        public int Quantidade { get; set; }

    }
}