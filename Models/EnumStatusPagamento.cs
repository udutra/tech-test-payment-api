namespace tech_test_payment_api.Models
{
    public enum EnumStatusPagamento
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoParaTransportadora = 2,
        Entregue = 3,
        Cancelada = 4
    }
}