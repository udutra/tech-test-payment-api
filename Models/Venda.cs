using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int VendedorId { get; set; }

        public Vendedor Vendedor { get; set; } = new Vendedor();

        public DateTime Data { get; set; }

        public EnumStatusPagamento Status { get; set; } = EnumStatusPagamento.AguardandoPagamento;

        public List<Item> ItensVendidos { get; set; } = new List<Item>();

    }
}